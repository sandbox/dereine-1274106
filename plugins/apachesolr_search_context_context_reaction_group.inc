<?php
/**
 * @file
 * Search context to display nodes of a certain node.
 */
class apachesolr_search_context_context_reaction_group extends apachesolr_search_context_context_reaction {
  var $apachesolr_search_context_type = 'group';

  function options_form($context) {
    return array('group' => array('#type' => 'value', '#value' => TRUE));
  }
  function execute(&$form, $form_state, $path = '') {
    if (parent::execute($form, $form_state, $path)) {
      if ($node = og_get_group_context()) {
        return 'im_og_gid:' . $node->nid;
      }
    }
  }
}
