<?php
/**
 * @file
 * Search Context to display nodes of a certain user.
 */

class apachesolr_search_context_context_reaction_user extends apachesolr_search_context_context_reaction {
  var $apachesolr_search_type = 'user';

  function execute(&$form, $form_state, $path = '') {
    if (parent::execute($form, $form_state, $path)) {
      // @TODO
      //   Is there a way to find out the uid from the context/ another api function?
      if (arg(0) == 'user' && is_numeric(arg(1))) {
        return 'uid:' . arg(1);
      }
    }
  }
}